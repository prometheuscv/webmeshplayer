/**
 * @author Neil Z. Shao, Hongyi Li
 * Prometheus 2021
 * https://www.prometh.xyz/
 */
import * as THREE from "../three.module.js";
import { SequenceLoader } from './SequenceLoader.js';
import { AudioManager } from './AudioLoader.js';

const createMeshMaterialWithTexture = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshBasicMaterial({
        transparent:isTransparent,
        opacity:0.5
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(40, 40, 40);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createLineMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshBasicMaterial({
        transparent:isTransparent,
        opacity:0.5,
        color: 0xffffff,
        wireframe: true
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            //child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createPointMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.PointsMaterial({
        transparent:isTransparent,
        opacity:0.5,
        color: 0xffffff,
        size: 0.5
    });
    var mesh = new THREE.Points(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createNormalMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshNormalMaterial({
        transparent:isTransparent,
        opacity:0.5
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

class MeshSequence {
    constructor(url) {
        this.root = new THREE.Object3D();
        this.isOpened = false;
        this.ready = false;
        this.isPlaying = false;
        this.isPlayOnce = false;
        this.frm_idx = 0;
        this.lastTime = new Date().getTime();
        this.displayMode = 'mesh';
        this.transparent = false;
        this.resolution = 512;

        // msg
        this.onLoad = null;
        this.onPlayProgress = null;
        this.onCacheProgress = null;
        this.onError = null;

        if (url)
            load(url);


        let self = this;
        this.updater = setInterval(() => {
            self.update();
        }, 10);
    }

    // load info json
    load(url, onLoad, onPlayProgress, onCacheProgress, onError) {
        let url_info = url + '/info.json';
        console.log('[MeshSequence] url_info =', url_info);

        fetch(url_info,{
            method: 'GET',
            mode: 'cors',
        })
        .then(res => res.json())
        .then((info) => {
            info.url = url;
            console.log('[MeshSequence] info =', info);
            this.open(info, onLoad, onPlayProgress, onCacheProgress, onError);
        })
        .catch(err => { console.log(err); });
    }

    // open loaders
    open(info, onLoad, onPlayProgress, onCacheProgress, onError) {
        this.info = info;
        this.N = info.number_frames - 1;

        this.onError = onError;
        this.onPlayProgress = onPlayProgress;
        this.onCacheProgress = onCacheProgress;

        console.log("............info.url",info.url);
        //const audio_url = './data/test.mp3';
        this.audioManager = new AudioManager(info.url + '/zhaiwu.mp3');

        let gType = info.geometry[0];
        this.geometryLoader = new SequenceLoader(this.N, info.url + '/' + gType, '.' + gType);
        this.geometryLoader.load();

        let tType = info.texture[0];
        //default atlas resolution 512
        this.textureLoader = new SequenceLoader(this.N, info.url + '/' + tType + '-' + this.resolution.toString(), '.' + tType);
        // console.log('this.textureLoader : ',this.textureLoader);
        this.textureLoader.load();

        this.isOpened = true;
        if (onLoad) {
            onLoad(this.root);
        }

    }

    changeMaterialMode(mode) {
        console.log("mode:" + mode)
        this.displayMode = mode;
    }

    // update current mesh
    updateCurrentMesh() {
        let geometry = this.geometryLoader.datas[this.frm_idx].geometry;
        let texture = this.textureLoader.datas[this.frm_idx].texture;
        if (!geometry || !texture) {
            // alert('not ready to update');
            console.log(geometry);
            console.log(texture);
            this.ready = false;
        }

        // replace mesh data
        if (this.mesh) {
            // texture.needsUpdate = true;
            this.mesh.material.map = texture;
            // this.mesh.material.needsUpdate = true;
            this.mesh.geometry = geometry;
            // this.mesh.needsUpdate = true;
        }
        else {
            if (this.displayMode == 'mesh') {
                let mesh = createMeshMaterialWithTexture(geometry, texture, this.transparent);
                this.replaceDisplayMesh(mesh);
            }
            if (this.displayMode == 'line') {
                let mesh = createLineMaterial(geometry, texture, this.transparent);
                this.replaceDisplayMesh(mesh);
            }
            if (this.displayMode == 'point') {
                let mesh = createPointMaterial(geometry, texture, this.transparent);
                this.replaceDisplayMesh(mesh);
            }
            if (this.displayMode == 'normal') {
                let mesh = createNormalMaterial(geometry, texture, this.transparent);
                this.replaceDisplayMesh(mesh);
            }
        }
    }

    replaceDisplayMesh(mesh) {
        for (var i = this.root.children.length - 1; i >= 0; i--) {
            this.root.remove(this.root.children[i]);
        }

        this.root.add(mesh);
        this.mesh = mesh;
    }

    // check ready
    checkReady() {
        // console.log('[MeshSequence] geometry ready =', this.geometryLoader.isBufferReady(this.frm_idx));
        // console.log('[MeshSequence] texture ready =', this.textureLoader.isBufferReady(this.frm_idx));

        if (this.ready) {
            if (!this.geometryLoader.isFrameReady(this.frm_idx) 
                || !this.textureLoader.isFrameReady(this.frm_idx)) 
            {
                this.ready = false;
                console.log('[MeshSequence] ready -> not ready');
            }
        }
        else {
            if (this.geometryLoader.isBufferReady(this.frm_idx) 
                && this.textureLoader.isBufferReady(this.frm_idx))
            {
                this.ready = true;
                console.log('[MeshSequence] not ready -> ready');
            }
        }
    }

    // step
    gotoFrame(frm_idx) {
        this.geometryLoader.update(frm_idx);
        this.textureLoader.update(frm_idx);
        this.frm_idx = frm_idx;
        this.isPlayOnce = true;
    }

    stepPrevious() {
        this.gotoFrame((this.frm_idx - 1 + this.N) % this.N);
        this.isPlayOnce = true;
    }

    stepNext() {
        this.gotoFrame((this.frm_idx + 1) % this.N);
        this.isPlayOnce = true;
    }

    // play
    play() {
        this.isPlaying = true;
    }

    pause() {
        this.isPlaying = false;
        //pause audio
        this.audioManager.audioPause();
    }

    changeMaterialMode(resolution)
    {
        this.resolution = resolution;
        if(this.textureLoader)
        {
            //console.info('this.textureLoader.url',this.textureLoader.url,'length ',this.textureLoader.url.length);
            var totalLens = this.textureLoader.url.length;
            var urlSplit = this.textureLoader.url.split('/');
            var urlLength = urlSplit.length;
            var urlResolution = urlSplit[urlLength-1];
            if(urlResolution.substring(0,3) == 'jpg')
            {
                var jpgDir = urlResolution.substring(0,3) + '-' + this.resolution.toString();
                var newUrl = this.textureLoader.url.substring(0, totalLens - urlResolution.length) + jpgDir;
                this.textureLoader.url = newUrl;
                console.log('.............New Url is ',this.textureLoader.url);
            }
            
            
        }
    }

    // update routine
    update() {
        // skip if not opened
        if (!this.isOpened)
            return;

        // skip if no playing
        if (!this.isPlaying && !this.isPlayOnce) {
            this.geometryLoader.update(this.frm_idx);
            this.textureLoader.update(this.frm_idx);
            return;
        }

        // skip update too fast
        let curTime = new Date().getTime();
        let fpsGap = 1000 / this.info.fps;
        if ((curTime - this.lastTime) < fpsGap) {
            this.geometryLoader.update(this.frm_idx);
            this.textureLoader.update(this.frm_idx);
            return;
        }

        // current ready -> goto next frame
        if (this.ready) {
            this.frm_idx = (this.frm_idx + 1) % this.N;
        }
        this.geometryLoader.update(this.frm_idx);
        this.textureLoader.update(this.frm_idx);

        //update progress
        var cache_idx = Math.min(this.geometryLoader.max_frm_idx, this.textureLoader.max_frm_idx)
        if(cache_idx < this.frm_idx)
        {
            this.onCacheProgress(1);
        }else
        {
            this.onCacheProgress(cache_idx / this.N);
        }

        // check ready
        this.checkReady();
        if (!this.ready){
            //if not ready pause audio 
            this.audioManager.audioPause();
            return;
        }

        //console.log('update');
        this.lastTime = curTime;

        // if ready and not playing update mesh
        this.audioManager.audioPlay();

        this.onPlayProgress(this.frm_idx / this.N);
        //console.log("................... cache is play idx:",cache_idx, this.frm_idx);

        //console.info("ready to updateCurrentmesh");
        this.updateCurrentMesh();
        this.isPlayOnce = false;
    }
} 

export { MeshSequence };
