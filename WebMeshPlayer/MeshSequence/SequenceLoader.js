/**
 * @author Neil Z. Shao, Hongyi Li
 * Prometheus 2021
 * https://www.prometh.xyz/
 */
import * as THREE from "../three.module.js";
import { DRACOLoader } from './DRACOLoader.js';

class FrameData {
    constructor(url, frm_idx) {
        this.url = url;
        this.idx = frm_idx;
        this.loading = false;
        this.loaded = false;
        
        this.texture = null;
        this.geometry = null;
    }

    dispose() {
        this.loaded = false;
        this.loading = false;
        if (this.texture)
            this.texture.dispose();
        if (this.geometry)
            this.geometry.dispose();
    }
}

class SequenceLoader {
    constructor(N, url, suffix) {
        this.N = N;
        this.url = url;
        this.suffix = suffix;

        this.numLoaders = 5;
        this.loaders = [];

        this.datas = [];
        this.maxNumLoading = 7;
        this.maxNumBufferForward = 75;
        this.maxNumBufferBackward = 15;
        this.minNumReady = 15;

        this.verbose = false;
        this.resolution = 512;

        this.max_frm_idx = 0;
    }

    // update loading based on current frame index
    update(frm_idx) {
        this.removeUnusedData(frm_idx);
        this.updateLoader(frm_idx);
    }

    // start loading a sequence from url
    load(onLoad, onProgress, onError) {
        this.onLoad = onLoad;
        this.onProgress = onProgress;
        this.onError = onError;
        this.update(0);
    }

    // remove data in buffer thats expired
    removeUnusedData(frm_idx) {
        // let scope = this;
        // let updated = true;
        // while (updated) {
        //     updated = false;
        //     scope.datas.forEach(function(item, index, object) {
        //         const bwdGap = (frm_idx - item.idx + scope.N) % scope.N;
        //         const fwdGap = (item.idx - frm_idx + scope.N) % scope.N;
        //         if (bwdGap > scope.maxNumBufferBackward && fwdGap > scope.maxNumBufferForward) {
        //             console.log('[SequenceLoader] remove data', item.idx);
        //             item.dispose();
        //             object.splice(index, 1);
        //             updated = true;
        //         }
        //     });
        // }

        let scope = this;
        scope.datas.forEach(function(item, index, object) {
            if (!item)
                return;

            const bwdGap = (frm_idx - item.idx + scope.N) % scope.N;
            const fwdGap = (item.idx - frm_idx + scope.N) % scope.N;
            if (bwdGap > scope.maxNumBufferBackward && fwdGap > scope.maxNumBufferForward) {
                if (scope.verbose)
                    console.log('[SequenceLoader] remove data', item.idx);
                item.dispose();
                scope.datas[item.idx] = null;
            }
        });
    }

    // current loading
    getCurrentLoading() {
        let count = 0;
        for (let i = 0; i < this.N; ++i) {
            if (this.datas[i] && this.datas[i].loading)
                count++;
        }
        return count;
    }

    // current loaded
    getCurrentLoaded() {
        let count = 0;
        for (let i = 0; i < this.N; ++i) {
            if (this.datas[i] && this.datas[i].loaded)
                count++;
        }
        return count;
    }

    // update loader to load more
    updateLoader(frm_idx) {
        let numLoading = this.getCurrentLoading();
        let numLoaded = this.getCurrentLoaded();
        if (this.verbose)
            console.log('[SequenceLoader] loading =', numLoading, 'loaded =', numLoaded);

        if (numLoading >= this.maxNumLoading)
            return;

        let newLoading = this.maxNumLoading - numLoading;
        if (newLoading <= 0)
            return;

        for (let i = 0; i <= this.maxNumBufferForward; ++i) {
            let idx = (i + frm_idx) % this.N;
            if (this.datas[idx] && (this.datas[idx].loading || this.datas[idx].loaded)){
                continue;
            }

            if (newLoading <= 0)
                break;

            const cur_url = this.url + '/' + idx + this.suffix;
            //console.log('...................cur_url is :',cur_url);
            if (this.verbose)
                console.log('[SequenceLoader] load url', cur_url);

            this.datas[idx] = new FrameData(cur_url, idx);
            this.datas[idx].loading = true;
            this.datas[idx].loaded = false;

            if (this.suffix == '.jpg') {
                const loader_idx = idx % this.numLoaders;
                if (!this.loaders[loader_idx]) {
                    this.loaders[loader_idx] = new THREE.TextureLoader();
                }

                const textureLoader = this.loaders[loader_idx];
                textureLoader.load(cur_url, 
                    (texture) => {
                        
                        if (this.verbose)
                            console.log('onload', cur_url);
                        this.datas[idx].texture = texture;
                        this.datas[idx].loading = false;
                        this.datas[idx].loaded = true;
                        //console.log("..........max jpg :",idx);
                        this.max_frm_idx = idx;
                    }
                );
            }
            else if (this.suffix == '.basis') {
                const loader_idx = idx % this.numLoaders;
                if (!this.loaders[loader_idx]) {
                    this.loaders[loader_idx] = new BasisTextureLoader();
                    this.loaders[loader_idx].detectSupport(renderer);
                    this.loaders[loader_idx].setOutputType('data');
                }

                const basisLoader = this.loaders[loader_idx];
                basisLoader.load(cur_url, 
                    (texture) => {
                        if (this.verbose)
                            console.log('onload', cur_url);
                        this.datas[idx].texture = texture;
                        this.datas[idx].loading = false;
                        this.datas[idx].loaded = true;
                    }
                );
            }
            else if (this.suffix == '.drc') {
                const loader_idx = idx % this.numLoaders;
                if (!this.loaders[loader_idx]) {
                    this.loaders[loader_idx] = new DRACOLoader();
                    this.loaders[loader_idx].setDecoderPath('https://www.gstatic.com/draco/v1/decoders/');
                }

                const dracoLoader = this.loaders[loader_idx];
                dracoLoader.load(cur_url,
                    (geometry) => {
                        if (this.verbose)
                            console.log('onload', cur_url);
                        this.datas[idx].geometry = geometry;
                        this.datas[idx].loading = false;
                        this.datas[idx].loaded = true;
                        this.max_frm_idx = idx;
                        //console.log("..........max drc :",idx);
                    }
                )
            }
            else {
                isLoading = false;
                return;
            }

            newLoading--;
        }
    }

    isFrameReady(frm_idx) {
        if (!this.datas[frm_idx] || !this.datas[frm_idx].loaded)
            return false;

        return true;
    }

    isBufferReady(frm_idx) {
        for (let i = 0; i < this.minNumReady; ++i) {
            //let idx = (frm_idx + this.minNumReady) % this.N;
            let idx = (frm_idx + i) % this.N;
            if (!this.datas[idx] || !this.datas[idx].loaded)
                return false;
        }

        return true;
    }

}

export { SequenceLoader };
