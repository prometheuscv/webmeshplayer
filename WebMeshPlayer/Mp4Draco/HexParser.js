const dict = {
    // 其中 65 - 70， 97 - 102 (a - f) 不区分大小写
    48: '0',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
    54: '6',
    55: '7',
    56: '8',
    57: '9',

    65: 'A', // A
    66: 'B', // B
    67: 'C', // C
    68: 'D', // D
    69: 'E', // E
    70: 'F', // F

    97: 'a', // a
    98: 'b', // b
    99: 'c', // c
    100: 'd', // d
    101: 'e', // e
    102: 'f', // f
    103: 'g', // g
    104: 'h', // h
    105: 'i', // i
    106: 'j', // j
    107: 'k', // k
    108: 'l', // l
    109: 'm', // m
    110: 'n', // n
    111: 'o', // o
    112: 'p', // p
    113: 'q', // q
    114: 'r', // r
    115: 's', // s
    116: 't', // t
    117: 'u', // u
    118: 'v', // v
    119: 'w', // w
    120: 'x', // x
    121: 'y', // y
    122: 'z', // z
}

const intFromArray = function (array) {
    let s1 = '0x' + array[0] + array[1]
    let s2 = '0x' + array[2] + array[3]
    let s3 = '0x' + array[4] + array[5]
    let s4 = '0x' + array[6] + array[7]

    let n1 = parseInt(s1)
    let n2 = parseInt(s2)
    let n3 = parseInt(s3)
    let n4 = parseInt(s4)
    let value = (n4 * 256 * 256 * 256) + (n3 * 256 * 256) + (n2 * 256) + n1
    return value
}

// uint8arrry to char array
const hex2str = function (array) {
    let str = []
    for (let i = 0; i < array.length; i++) {
        // 从 buffer 里取出元素，hex 就变成 int
        let k = array[i]
        // 将 int 转成对应的 str
        let n = dict[k]
        str.push(n)
    }
    return str
}

const parseLengh = function (len_buffer) {
    let str = hex2str(len_buffer)
    let length = intFromArray(str)
    return length
}

const parseTag = function (tag_buffer) {
    let str = hex2str(tag_buffer)
    let tag = str.join("")
    return tag
}

const arrayEqual = function (arr1, arr2) {
	if (arr1.length != arr2.length) {
		return false
	}
	for (var i = 0; i < arr1.length; ++i) {
		if (arr1[i] !== arr2[i]) {
			return false
		}
	}
	return true
};

export { intFromArray, hex2str, parseLengh, parseTag, arrayEqual };
