/**
 * @author mrdoob / http://mrdoob.com/
 */
import * as THREE from "../three.module.js";
import { DRACOLoader } from './DRACOLoader.js';
import * as HexParser from "./HexParser.js";

const createMeshMaterialWithTexture = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshBasicMaterial({
        transparent:isTransparent,
        opacity:0.5
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createLineMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshBasicMaterial({
        transparent:isTransparent,
        opacity:0.5,
        color: 0xffffff,
        wireframe: true
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            //child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createPointMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.PointsMaterial({
        transparent:isTransparent,
        opacity:0.5,
        color: 0xffffff,
        size: 0.5
    });
    var mesh = new THREE.Points(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

const createNormalMaterial = function (geometry, texture,isTransparent) {
    var material = new THREE.MeshNormalMaterial({
        transparent:isTransparent,
        opacity:0.5
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.scale.set(30, 30, 30);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    let b = Math.PI / 2;
    mesh.rotation.x = 2 * b;

    mesh.traverse(function (child) {
        if (child.isMesh) {
            child.material.map = texture;
            child.material.needsUpdate = true;
        }
    });
    return mesh;
}

class MeshQueue {
    constructor() {
        // root object to add to scene
        this.root = new THREE.Object3D();
        this.displayMode = 'mesh';
        this.ready = false;
        this.isPlaying = false;
        this.autoPlay = true;
        this.transparent = false;

        // mesh queue
        this.length = 0;
        this.geometries = [];
    }

    dispose() {
        for (let i = 0; i < this.geometries.length; ++i)
            this.geometries[i].dispose();
        this.loader.dispose();

        this.ready = false;
        this.isPlaying = false;
        this.length = 0;
        this.geometries = [];
    }

    load(url, onLoad, onProgress, onError) {
        this.loader = new Mp4MeshLoader();
        this.loader.isPlaying = false;

        this.loader.load(url, this, function () {
            this.ready = true;
            if (this.autoPlay) {
                this.isPlaying = true;
                this.loader.player.isPlaying = true;
            }
            else {
                this.isPlaying = false;
                this.loader.player.isPlaying = false;
            }
            onLoad(this);
        }.bind(this), onProgress, onError);
    }

    play() {
        this.isPlaying = true;
        this.loader.player.isPlaying = true;
        console.log('[MeshQueue] play');
    }

    pause() {
        this.isPlaying = false;
        this.loader.player.isPlaying = false;
        console.log('[MeshQueue] pause');
    }

    setGeometry(idx, geometry) {
        this.geometries[idx] = geometry;
        this.length = this.geometries.length;
    }

    setDisplayTexture(idx, texture) {
        let geometry = this.geometries[idx];

        if (this.displayMode == 'mesh') {
            let mesh = createMeshMaterialWithTexture(geometry, texture,this.transparent);
            this.replaceDisplayMesh(mesh);
        }
        if (this.displayMode == 'line') {
            let mesh = createLineMaterial(geometry, texture,this.transparent);
            this.replaceDisplayMesh(mesh);
        }
        if (this.displayMode == 'point') {
            let mesh = createPointMaterial(geometry, texture,this.transparent);
            this.replaceDisplayMesh(mesh);
        }
        if (this.displayMode == 'normal') {
            let mesh = createNormalMaterial(geometry, texture,this.transparent);
            this.replaceDisplayMesh(mesh);
        }
    }

    replaceDisplayMesh(mesh) {
        for (var i = this.root.children.length - 1; i >= 0; i--) {
            this.root.remove(this.root.children[i]);
        }

        this.root.add(mesh);
    }

    changeMaterialMode(mode) {
        console.log("mode:" + mode)
        this.displayMode = mode;
    }

}

const resizeUint8Array = function (buffer, src_w, src_h, dst_w, dst_h) {

    var scale_w = src_w / dst_w;
    var scale_h = src_h / dst_h;

    var size = dst_w * dst_h;
    var data = new Uint8Array(3 * size);

    for (var y1 = 0; y1 < dst_h; y1++) {
        var y0 = Math.round(y1 * scale_h);
        y0 = Math.min(Math.max(y0, 0), src_h - 1);

        for (var x1 = 0; x1 < dst_w; x1++) {
            var x0 = Math.round(x1 * scale_w);
            x0 = Math.min(Math.max(x0, 0), src_w - 1);

            data[((y1 * dst_w) + x1) * 3 + 0] = buffer[((y0 * src_w) + x0) * 4 + 0];
            data[((y1 * dst_w) + x1) * 3 + 1] = buffer[((y0 * src_w) + x0) * 4 + 1];
            data[((y1 * dst_w) + x1) * 3 + 2] = buffer[((y0 * src_w) + x0) * 4 + 2];
            // data[((y1 * dst_w) + x1) * 4 + 2] = buffer[((y0 * src_w) + x0) * 4 + 2];
        }
    }

    return data;
}

var Mp4MeshLoader = (function () {
    function Mp4MeshLoader() {
        var manager = new THREE.LoadingManager();
        manager.onProgress = function (item, loaded, total) {
            // console.log(item, loaded, total);
        };

        THREE.Loader.call(this, manager);
        this.loop = true;

        this.defaultAttributeIDs = {
            position: 'POSITION',
            normal: 'NORMAL',
            color: 'COLOR',
            uv: 'TEX_COORD'
        };
        this.defaultAttributeTypes = {
            position: 'Float32Array',
            normal: 'Float32Array',
            color: 'Float32Array',
            uv: 'Float32Array'
        };
    }

    Mp4MeshLoader.prototype = Object.assign(Object.create(THREE.Loader.prototype), {
        constructor: Mp4MeshLoader,

        dispose: function() {
            if (this.player) {
                this.quit = true;
                this.player.quit = true;
            }
        },

        load: function (url, meshQueue, onLoad, onProgress, onError) {
            let scope = this;
            scope.onProgress = onProgress;
            scope.onError = onError;

            scope.meshProgress = 0;
            scope.textureProgress = 0;

            var loader = new THREE.FileLoader(this.manager);
            loader.setPath(this.path);
            loader.setResponseType('arraybuffer');
            loader.load(url, function (data) {
                let bytes = new Uint8Array(data)

                // assemble all meshes data to get total number
                let meshesArraybuffer = scope.parseMp4(bytes, onError);
                let numFrames = meshesArraybuffer.length;
                console.log('[Mp4MeshLoader] ', numFrames, ' frames');

                let startTime0 = new Date().getTime();
                scope.parseMp4Draco(meshesArraybuffer, meshQueue, function () {
                    let endTime = new Date().getTime();
                    console.log('[Mp4MeshLoader] draco decode finished ', (endTime - startTime0), 'ms');

                    let startTime1 = new Date().getTime();
                    scope.parseMp4TextureBroadway(bytes, numFrames, meshQueue, function () {
                        let endTime = new Date().getTime();
                        console.log('[Mp4MeshLoader] parse texture finished ', (endTime - startTime1), 'ms');
                    }, (xhr) => { scope.onProgressInternal(xhr); }, onError);

                    onLoad();
                }, (xhr) => { scope.onProgressInternal(xhr); }, onError);

            }, (xhr) => { scope.onProgressInternal(xhr); }, onError);
        },

        onProgressInternal: function (xhr) {
            let scope = this;

            if (xhr.lengthComputable) {
                let percentComplete = xhr.loaded / xhr.total * 100;
                // console.log('file ' + Math.round(percentComplete, 2) + '% downloaded');
                xhr.loading = Math.round(percentComplete, 2) * 0.3;
            }
            else {
                if (xhr.isMeshLoading) {
                    let percentComplete = xhr.loaded / xhr.total * 100;
                    // console.log('model ' + Math.round(percentComplete, 2) + '% parsed');
                    scope.meshProgress = percentComplete;
                }
                if (xhr.isTextureLoading) {
                    let percentComplete = xhr.loaded / xhr.total * 100;
                    // console.log('texture ' + Math.round(percentComplete, 2) + '% parsed');
                    scope.textureProgress = percentComplete;
                }

                // xhr.loading = 30 + Math.round(scope.meshProgress, 2) * 0.2 + Math.round(scope.textureProgress, 2) * 0.5;
                xhr.loading = 30 + Math.round(scope.meshProgress, 2) * 0.7;
            }

            scope.onProgress(xhr.loading);
        },

        parseMp4: function (bytes, onError) {

            // 查找所有的标志 uid
            let uid = [0xca, 0x38, 0xb7, 0xe7];
            let meshesArraybuffer = [];

            for (let i = 0; i < bytes.length - uid.length; i++) {
                let tmp_tag = [bytes[i], bytes[i + 1], bytes[i + 2], bytes[i + 3]];
                let found = HexParser.arrayEqual(tmp_tag, uid);

                // 找到
                if (found) {
                    // console.log('found uid');
                    // 解析sei，
                    // sei 有 3 部分组成，uid ：长度 16； obj length：长度  8； obj data： 长度  由 obj length 计算得来
                    // obj length
                    let start = i + 16 + 8;
                    let length_buffer = bytes.slice(start, start + 8);
                    let length = HexParser.parseLengh(length_buffer);
                    var tag = bytes.slice(start + 8, start + 12);

                    // obj data
                    //byteString or Draco
                    let dr64_tag = [0x64, 0x72, 0x36, 0x34];
                    if (HexParser.arrayEqual(tag, dr64_tag)) {
                        // console.log("dr64!");
                        var data_buffer = bytes.slice(start + 16, start + length);
                        var base64_str = new TextDecoder("utf-8").decode(data_buffer);

                        let decoded_str = window.atob(base64_str);
                        var buffer = new Uint8Array(decoded_str.length);
                        for (var k in buffer)
                            buffer[k] = decoded_str[k].charCodeAt();

                        // push to list
                        meshesArraybuffer.push(buffer);

                    } else {
                        var string = new TextDecoder("utf-8").decode(tag);
                        console.log("invalid tag:", string);
                        // var data = bytes.slice(start, start + length);
                        // s = parseData(obj_data)
                    }

                }
            }

            return meshesArraybuffer;
        },

        parseMp4Draco: function (meshesArraybuffer, meshQueue, onLoad, onProgress, onError) {

            var dracoLoader = new DRACOLoader();
            dracoLoader.setDecoderPath('https://www.gstatic.com/draco/v1/decoders/');

            var numLoaded = 0;
            for (let i = 0; i < meshesArraybuffer.length; i++) {
                // decode in worker
                var taskConfig = {
                    attributeIDs: this.defaultAttributeIDs,
                    attributeTypes: this.defaultAttributeTypes,
                    useUniqueIDs: false,
                    frm_idx: i,
                };

                dracoLoader.decodeGeometry(meshesArraybuffer[i].buffer, taskConfig)
                    // receive decoded mesh
                    .then(function (geometry) {

                        // console.log('[Mp4MeshLoader] draco loaded ', geometry.frm_idx);
                        geometry.computeVertexNormals();
                        meshQueue.setGeometry(geometry.frm_idx, geometry);

                        numLoaded += 1;
                        let progress = {
                            isMeshLoading: true,
                            total: meshesArraybuffer.length,
                            loaded: numLoaded
                        };
                        onProgress(progress);

                        if (numLoaded == meshesArraybuffer.length) {
                            console.log('[Mp4MeshLoader] all mesh decoded');
                            onLoad();
                        }

                    })
                    .catch(onError);
            }

        },

        parseMp4TextureBroadway(bytes, numFrames, meshQueue, onLoad, onProgress, onError) {

            let scope = this;
            // let player = new MP4Player(new Stream(url), true, false);
            let player = new MP4Player(new Bytestream(bytes), true, false);
            scope.player = player;

            player.avc.onPictureDecoded = function (buffer, width, height, infos) {
                if (scope.quit) {
                    console.log('[Mp4MeshLoader] quit');
                    return;
                }

                // console.log(infos);
                var data = resizeUint8Array(buffer, width, height, 512, 512);

                // var idx = numLoaded;
                let idx = infos[0].frm_idx;
                let texture = new THREE.DataTexture(data, 512, 512, THREE.RGBFormat);
                texture.flipY = true;
                meshQueue.setDisplayTexture(idx, texture);

                if (idx == numFrames - 1 && scope.loop) {
                    setTimeout(() => {
                        player.play();
                    }, 1);
                }
            };

            player.play();
        },

        loadMp4Texture: function (url, meshQueue, onLoad, onProgress, onError) {

            let scope = this;
            scope.video = document.createElement('video');
            let video = scope.video;
            video.width = 1024;
            video.height = 1024;
            video.muted = true;
            video.autoplay = true;
            video.src = url;
            video.crossOrigin = 'anonymous';
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');

            video.onloadeddata = function () {
                video.pause();
                video.loop = false;
                video.currentTime = 0.0;
                setTimeout(() => {
                    scope.loopTextures(meshQueue, onLoad, onProgress, onError);
                }, 100);
            };

        },

        loopTextures: function (meshQueue, onLoad, onProgress, onError) {

            let scope = this;
            let video = scope.video;

            console.log(document);
            var canvas = document.createElement('canvas');
            console.log(canvas);
            canvas.width = 1024;
            canvas.height = 1024;
            var ctx = canvas.getContext('2d');
            console.log(ctx);
            ctx.fillStyle = '#FF0000';
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            var cur_time = video.currentTime;
            var idx = Math.round((cur_time - 0.033) / 0.033);
            console.log("update %f, %d", cur_time, idx);

            if (idx > 99) {
                idx -= 1;
            }

            ctx.drawImage(video, 0, 0);
            var tt = new THREE.Texture(canvas);
            tt.needsUpdate = true;

            meshQueue.setTexture(idx, tt);

            video.currentTime = cur_time + 0.033;
            if (cur_time < video.duration) {
                setTimeout(() => {
                    scope.loopTextures(meshQueue, onLoad, onProgress, onError);
                }, 10);
            }
            else {
                onLoad(meshQueue);
            }
        }

    });

    return Mp4MeshLoader;

})();

export { Mp4MeshLoader, MeshQueue };